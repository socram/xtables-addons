.PP
The \fBEVIL\fP is a mangle target that sets the IPv4 evil bit, as per the
RFC 3514 standard <https://www.rfc-editor.org/rfc/rfc3514>.
.PP
EVIL takes no options.
