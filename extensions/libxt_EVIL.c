/*
 *	Evil bit (RFC 3514) target extension for Xtables
 *	Copyright © 2022 Marcos Del Sol Vives <marcos@orca.pet>
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License; either
 *	version 2 of the License, or any later version, as published by the
 *	Free Software Foundation.
 */
#include <getopt.h>
#include <stdio.h>
#include <xtables.h>

static void evil_tg_help(void)
{
	printf("EVIL takes no options\n");
}

static int evil_tg_parse(int c, char **argv, int invert, unsigned int *flags,
    const void *entry, struct xt_entry_target **match)
{
	return 0;
}

static void evil_tg_check(unsigned int flags)
{
}

static struct xtables_target evil_tg_reg = {
	.version       = XTABLES_VERSION,
	.name          = "EVIL",
	.revision      = 0,
	.family        = NFPROTO_IPV4,
	.help          = evil_tg_help,
	.parse         = evil_tg_parse,
	.final_check   = evil_tg_check,
};

static void __attribute__((constructor)) evil_tg_ldr(void)
{
	xtables_register_target(&evil_tg_reg);
}
