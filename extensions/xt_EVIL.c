/*
 *	Evil bit (RFC 3514) target extension for Xtables
 *	Copyright © 2022 Marcos Del Sol Vives <marcos@orca.pet>
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License; either
 *	version 2 of the License, or any later version, as published by the
 *	Free Software Foundation.
 */
#include <linux/ip.h>
#include <linux/module.h>
#include <linux/skbuff.h>
#include <linux/netfilter/x_tables.h>

static unsigned int
evil_tg(struct sk_buff *skb, const struct xt_action_param *par)
{
	struct iphdr *iph;
	__be16 orig_frag;

	if (skb_ensure_writable(skb, sizeof(*iph))) {
		return NF_DROP;
	}

	iph = ip_hdr(skb);
	orig_frag = iph->frag_off;
	iph->frag_off = orig_frag | htons(0x8000);
	csum_replace2(&iph->check, orig_frag, iph->frag_off);

	return XT_CONTINUE;
}

static struct xt_target evil_tg_reg = {
	.name      = "EVIL",
	.revision  = 0,
	.family    = NFPROTO_IPV4,
	.table     = "mangle",
	.target    = evil_tg,
	.me        = THIS_MODULE,
};

static int __init evil_tg_init(void)
{
	return xt_register_target(&evil_tg_reg);
}

static void __exit evil_tg_exit(void)
{
	xt_unregister_target(&evil_tg_reg);
}

module_init(evil_tg_init);
module_exit(evil_tg_exit);
MODULE_DESCRIPTION("Xtables: Evil bit (RFC 3514) setting");
MODULE_AUTHOR("Marcos Del Sol Vives <marcos@orca.pet>");
MODULE_LICENSE("GPL");
MODULE_ALIAS("ipt_EVIL");
